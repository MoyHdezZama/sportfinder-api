<!DOCTYPE html>
<html>
    <head>
        <title>SpotFinder</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        @include('emails.partials.css')
    </head>
    <body>
        <div class="content">
            <header>
                <!-- TODO: Set image, link, and privacy -->
                <!-- <img src="http://cadeci.testingweb.mx/admin_assets/images/brand.png" alt="Cadeci" class="logo" /> -->

                @yield('content')

                <p>&nbsp;</p>
                <p>
                    <small>
                        Este correo fue generado automáticamente. Por favor no conteste este mensaje.
                        La información contenida en este mensaje y en cualquier archivo o documento que se adjunte al mismo
                        es confidencial. Está dirigida exclusivamente al uso del destinatario y no debe ser utilizado por
                        otra persona. Si usted recibe esta transmisión por error, por favor notifique al
                        remitente por esta vía.<br />
                        <a href="{{ config('mail.link_web') }}" target="_blank">spotfinder.com</a>
                        |
                        <a href="{{ config('mail.link_privacy') }}" target="_blank">Aviso de privacidad</a>
                    </small>
                </p>
            </header>
        </div>
    </body>
</html>
