@extends('emails.layout.default')

@section('content')

<h2>Estimado(a) {{ $name }}:</h2>
<p>
    Recibimos una solicitud para que puedas recuperar tu cuenta en <strong>SpotFinder app</strong>,
    por lo que deberás ingresar el siguiente código para crear una nueva contraseña:
</p>
<h3><span>Código:</span> {{ $code }}</h3>

@endsection
