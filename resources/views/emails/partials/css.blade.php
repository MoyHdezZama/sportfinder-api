<style>
    .content{
        display: block; margin: 50px auto; width: 600px; padding: 50px 80px;
        font-family: "Open Sans", sans-serif;
    }
    .logo{
        max-width: 100%; display: block; clear: both;
    }
    h2,
    h3{
        color: #333333;
    }
    h3{
        font-size: 28px;
    }
    h3 span{
        font-weight: 200; color: #333333;
    }
    p{
        color: #333333;
    }
    small{
        font-size: 11px; color: #333333; margin-top: 30px;
    }
    a{
        color: #333333;
    }
    button{
        display: inline-block; background: #333333; width: auto; height: auto; line-height: 36px;
        padding: 0 40px; color: #fff; text-decoration: none; border-radius: 5px; border: none;
        font-size: 14px;
    }

    @media screen and (max-width: 600px){
        .content{
            margin: 40px 5%; width: 90%; max-width: 100%; padding: 20px 0;
        }
        .logo{
            width: 75px;
        }
        h2{
            font-size: 16px;
        }
        h3{
            font-size: 20px;
        }
        p{
            font-size: 12px;
        }
        small{
            font-size: 10px; margin-top: 20px;
        }
    }

</style>
