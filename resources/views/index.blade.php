<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SpotFinder</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/landing/main.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Anton|Montserrat:300">
</head>
<body>

<div class="introduction1">
    <!-- NAV -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <a class="navbar-brand p-0 pt-1 pl-3" href="#">
                <img src="{{ asset('images/landing/logo.png') }}" class="img-fluid" alt="Spotfinder logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-md-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item" id="nav-characteristics">
                        <a class="nav-link font-weight-bold pl-1" href="#">CARACTERÍSTICAS <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item" id="nav-how-works">
                        <a class="nav-link font-weight-bold pl-1" href="#">¿COMO FUNCIONA?</a>
                    </li>
                    <li class="nav-item" id="nav-contact">
                        <a class="nav-link font-weight-bold pl-1" href="#">CONTACTO</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- INTRODUCTION -->
    <section class="container-fluid">
        <div class="row d-flex">
            <div class="col-12 col-md-6 d-md-flex flex-md-column align-items-md-center p-0 introduction">
                <h1 class="p-3 col-12 col-md-6 col-lg-8 p-md-0">Excepteur sint occaecat cupidatat noent.</h1>
                <div class="intro-social-links col-12 col-md-8 col-lg-8 p-0 d-flex justify-content-between ">
                    <div class="col-5 col-md-6">
                        <img src="{{ asset('images/landing/app_store.png') }}"
                             class="img-fluid mr-md-2" alt="SpotFinder App Store">
                    </div>
                    <div class="col-5 col-md-6">
                        <img src="{{ asset('images/landing/google_play.png') }}"
                             class="img-fluid mr-md-2" alt="SpotFinder Google Play">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 my-4 d-flex justify-content-center">
                <img src="{{ asset('images/landing/iphone-x.png') }}" class="img-fluid" alt="Spot finder iPhone X">
            </div>
            <div class="col-12 col-md-8 offset-md-2 my-5 text-md-center">
                <h2>Lorem ipsum dolor sit amet, consec iscing elit</h2>
                <p class="mt-3">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla
                    pariatur.
                    Excepteur sint occaecat cupidatat non proident,</p>
            </div>
        </div>
    </section>
</div>

<!-- SECTION 2 -->
<section class="container-fluid my-lg-5 section2 section2-background-text">
    <div class="row">
        <div class="col-12 col-md-3 p-0">
            <div class="col-12 my-1 my-md-4">
                <h3 class="text-md-right">Duis aute irure</h3>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.</p>
            </div>
            <div class="col-12 my-1 my-md-5">
                <h3 class="text-md-right">consectetur adipisci</h3>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.</p>
            </div>
        </div>
        <div class="col-12 col-md-6 p-0">
            <div class="col-12 my-1 section2-iphone">
            </div>
        </div>
        <div class="col-12 col-md-3 p-0">
            <div class="col-12 my-1 my-md-4">
                <h3>Excepteur sint occaecat</h3>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.</p>
            </div>
            <div class="col-12 my-1 my-md-5 section2-background-text">
                <h3>Duis aute irure</h3>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.</p>
            </div>
        </div>
    </div>
</section>

<!-- SECTION 3 -->
<section class="container-fluid mt-2 mt-lg-5">
    <div class="row">
        <div class="col-12 col-md-7 p-0 my-2">
            <img src="{{ asset('images/landing/section3-skater.png') }}"
                 srcset="{{ asset('images/landing/section3-skater-large.png') }} 992w"
                 class="img-fluid" alt="Spot Finder Roller">
        </div>
        <div class="col-12 col-md-5 p-0 justify-content-center align-content-center">
            <div class="col-12 mt-4 section3-title">
                <h2>Lorem ipsum dolor sit amet, consec iscing elit</h2>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.
                    Excepteur sint occaecat cupidatat non proident,</p>
            </div>
            <div class="col-12 p-0 section3-iphone">

            </div>
            <div class="col-12 d-md-none">
                <h2>Lorem ipsum dolor sit amet, consec iscing elit</h2>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur.
                    Excepteur sint occaecat cupidatat non proident,</p>
            </div>
        </div>
    </div>
</section>

<!-- SECTION 4 -->
<section class="container-fluid mt-md-5">
    <div class="row">
        <div class="col-md-4 d-none d-md-block text-right">
            <img src="{{ asset('images/landing/section4-skateboard.png') }}"
                 class="img-fluid" alt="Skateboard">
            <h3 class="mt-5">Lorem ipsum dolor sit amet, consec iscing elit</h3>
            <p class="mt-4">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                pariatur.
                Excepteur sint occaecat cupidatat non proident.</p>
        </div>
        <div class="col-12 col-md-4 my-4 d-flex justify-content-center">
            <img src="{{ asset('images/landing/section4-iphone.png') }}" class="img-fluid"
                 alt="Spotfinder on iphone">
        </div>
        <div class="col-md-4 d-none d-md-flex flex-md-column justify-content-center mt-md-4">
            <img src="{{ asset('images/landing/section4-roller.png') }}"
                 class="img-fluid align-self-baseline" alt="Roller">
            <h3 class="mt-4">Lorem ipsum dolor sit amet, consec iscing elit</h3>
            <p class="mt-3">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                pariatur.
                Excepteur sint occaecat cupidatat non proident.</p>
        </div>
        <div class="col-12 d-md-none">
            <img src="{{ asset('images/landing/section4-roller.png') }}"
                 class="img-fluid mb-4" alt="">
            <h2>Lorem ipsum dolor sit amet, consec iscing elit</h2>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident,</p>
        </div>
    </div>
</section>

<!-- BANNER -->
<section class="container-fluid banner-background">
    <div class="row pt-5 pb-4">
        <div class="col-12 text-md-center px-md-5">
            <h2 class="mt-3 mb-3 ">Lorem ipsum dolor sit amet, consec iscing elit</h2>
            <p class="my-2 mx-md-5">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla
                pariatur. Excepteur sint occaecat cupidatat non proident,</p>
        </div>
        <div class="col-12 mt-3 px-3 d-flex justify-content-between justify-content-md-center"
             style="max-height: 48px;">
            <div class="col-6 col-md-2">
                <img src="{{ asset('images/landing/app_store.png') }}"
                     class="img-fluid mr-md-2" alt="SpotFinder App Store">
            </div>
            <div class="col-6 col-md-2">
                <img src="{{ asset('images/landing/google_play.png') }}"
                     class="img-fluid mr-md-2" alt="SpotFinder Google Play">
            </div>
        </div>
    </div>
</section>

<!-- CONTACT -->
<section class="container-fluid contact">
    <div class="row">
        <div class="col-12 col-md-7">
            <h2 class="mt-5 mb-0 mb-md-3 contact-background-text">Contacto</h2>
            <p class="contact-paragraph">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla
                pariatur. Excepteur sint occaecat cupidatat non proident,</p>
            <form action="" class="d-flex flex-column justify-content-center align-content-center contact-form">
                <input type="text" placeholder="Nombre">
                <div class="row m-0 d-md-flex justify-content-md-between">
                    <input type="text" class="col-12" placeholder="Email">
                    <input type="text" class="col-12" placeholder="Télefono">
                </div>
                <textarea name="" id="" placeholder="Mensaje" cols="30" rows="5"></textarea>
                <div class="row m-0">
                    <button class="col-12 col-md-5" type="submit">Enviar</button>
                </div>
            </form>
        </div>
        <div class="d-none d-md-block col-md-5">
            <img src="{{ asset('images/landing/obeja-roller-contact.png') }}"
                 class="img-fluid" alt="Obeja Roller Contacto">
        </div>
    </div>
</section>

<!-- FOOTER -->
<footer class="container mt-4">
    <div class="row">
        <div class="col-12 d-flex justify-content-center align-items-center">
            <span class="contact-span">Todos los derechos reservados</span>
            <span class="contact-span">|</span>
            <span class="contact-span">Aviso de privacidad</span>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/landing.js') }}"></script>
</body>
</html>