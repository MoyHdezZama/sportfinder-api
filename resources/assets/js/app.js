require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router';
import client from 'axios';
import Vuetify from 'vuetify';
import VeeValidate, {Validator} from 'vee-validate';

import messagesES from 'vee-validate/dist/locale/es';

import {routes} from './router/index';
import {store} from './store/store';

import Admin from './components/Admin';

Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VeeValidate);
Validator.localize('es', messagesES);

const router = new VueRouter({
    mode: 'history',
    routes
});

const app = new Vue({
    store,
    router,
    el: '#app',
    render: h => h(Admin)
});
