import Login from '../components/Login.vue';
import Admin from '../components/Admin.vue';
import UsersList from '../components/User/List.vue';

export const routes = [
    { path: '/', component: Login, name: 'Login' },
    { path: '/admin', component: Admin, name: 'Home' },
    { path: '/admin/users', component: UsersList, name: 'Users' }
];