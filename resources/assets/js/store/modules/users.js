import Vue from 'vue';
import config from '../../config';

export const users = {
    namespaced: true,
    state: {
        users: [],
        response: {},
        loading: false,
        error: false,
    },
    getters: {
        loading: function (state) {
            return state.loading;
        },
        users: function (state) {
            return state.users;
        },
        response: function (state) {
            return state.response;
        }
    },
    mutations: {
        FETCHING(state) {
            state.loading = true;
        },
        FETCHING_SUCCESS(state, payload) {
            state.response = payload.data;
            state.users = payload.data.data;
            state.loading = false;
        },
        FETCHING_ERROR(state) {
            state.loading = false;
            state.error = true;
        },
        REMOVING(state) {
            state.loading = true;
        },
    },
    actions: {
        fetch({commit, state}, payload) {
            commit('FETCHING');

            axios.get('/api/admin/users', {
                params: {
                    grant_type: config.GRANT_TYPE,
                    client_id: config.CLIENT_ID,
                    client_secret: config.CLIENT_SECRET
                }
            }).then((response) => {
                commit('FETCHING_SUCCESS', response);
            }).catch((error) => {
                commit('FETCHING_ERROR');
            });

        },
        destroy({commit, state}, payload) {
            commit('REMOVING');

            console.log(payload);

            axios.delete(`/api/admin/users/${payload.id}`, {
                params: {
                    grant_type: config.GRANT_TYPE,
                    client_id: config.CLIENT_ID,
                    client_secret: config.CLIENT_SECRET
                }
            }).then((response) => {
                commit('FETCHING_SUCCESS', response);
            }).catch((error) => {
                commit('FETCHING_ERROR');
            });

        }
    }
};