import Vue from 'vue';
import config from '../../config';

export const auth = {
    namespaced: true,
    state: {
        user: {},
        response: {}
    },
    getters: {
        user: function (state) {
            return state.user;
        }
    },
    mutations: {
        USER_COMMIT(state, payload) {
            state.user = payload;
        },
        LOGIN(state) {
            state.pending = true;
        },
        LOGIN_SUCCESS(state) {
            state.pending = false;
        },
        LOGOUT(state) {
        },
    },
    actions: {
        login({commit}, payload) {
            commit('LOGIN');

            return new Promise((resolve, reject) => {
                axios.post('/oauth/token', {
                    grant_type: config.GRANT_TYPE,
                    client_id: config.CLIENT_ID,
                    client_secret: config.CLIENT_SECRET,
                    username: payload.username,
                    password: payload.password
                }).then((response) => {
                    resolve(response)
                }).catch((error) => {
                    console.log(error);
                    reject(error.response)
                });

            });
        },
        logout({commit}) {
            //localStorage.removeItem("token");
            commit('LOGOUT');
        }
    }
};