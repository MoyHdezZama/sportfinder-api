<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    $url = 'http://201.151.228.153:9080/gsautos-wsDesa/soap/autenticacionWS?wsdl';
    $url = 'http://201.151.228.153:9080/gsautos-wsDesa/soap/cotizacionEmisionWS?wsdl';

    try {
        $client = new SoapClient($url);
        dd($client->__getFunctions(),$client->__getTypes());

        dd($client->obtenerToken(['arg0' =>['password' => '2r2kGdeUA0','usuario' => 'ATC0',]]));


    } catch (SoapFault $fault) {
        dd($fault);
    }
    dd("test");
});*/


Route::get(
    '/',
    function () {
        return view('index');
    }
);

Route::get(
    '/admin/{vue_capture?}',
    function () {
        return view('admin.index');
    }
)->where('vue_capture', '[\/\w\.-]*');

Auth::routes();
