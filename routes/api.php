<?php

use Illuminate\Http\Request;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(
    ['namespace' => 'Api\V1'],
    function () {
        Route::group(
            ['prefix' => 'account'],
            function () {
                Route::post('testPush', 'AccountController@testPush');

                Route::get('favorites', 'SpotController@userFavorites');
                Route::get('spots', 'SpotController@mySpots');

                Route::post('register', 'AccountController@store');
                Route::post('logout', 'AccountController@logout');
                Route::get('profile', 'AccountController@profile');
                Route::post('update', 'AccountController@profileUpdate');
                Route::post('recover', 'AccountController@recoverRequest');
                Route::post('updatePassword', 'AccountController@updatePassword');
                Route::post('updateFCMToken', 'AccountController@updateFCMToken');
            }
        );

        Route::get('spots/filter', 'SpotController@filter');
        Route::get('spots/map', 'SpotController@mapSpots');
        Route::post('spots/{spot}/favorites', 'SpotController@addToFavorites');
        Route::post('spots/{spot}/comments', 'SpotController@addComment');
        Route::get('spots/{spot}/comments', 'SpotController@getComments');
        Route::Resource('spots', 'SpotController');
        Route::Resource('categories', 'CategoryController');

        Route::group(
            ['prefix' => 'admin'],
            function () {
                Route::apiResource('users', 'UserController');
            }
        );
    }
);
