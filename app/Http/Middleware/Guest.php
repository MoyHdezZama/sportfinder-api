<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Guest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user() && (Auth::user()->role_id != 2))
        {
            return $next($request);
        }

        return response()->json([
            'code' => 421,
            'message' => 'This function is not available for guests'
        ]);
    }
}
