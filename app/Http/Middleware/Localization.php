<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = ($request->hasHeader('locale')) ? $request->header('locale') : 'es';

        if (!in_array($locale, config('translatable.locales'))) {
            $locale = 'es';
        }

        app()->setLocale($locale);
        Carbon::setLocale($locale);

        return $next($request);
    }
}
