<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use Socialite;
use Adaojunior\Passport\SocialGrantException;
use Adaojunior\Passport\SocialUserResolverInterface;

class SocialUserResolver implements SocialUserResolverInterface
{

    /**
     * Resolves user by given network and access token.
     *
     * @param string $network
     * @param string $accessToken
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function resolve($network, $accessToken, $accessTokenSecret = null)
    {
        switch ($network) {
            case 'facebook':
                return $this->authWithFacebook($accessToken);
                break;
            default:
                throw SocialGrantException::invalidNetwork();
                break;
        }
    }

    /**
     * Resolves user by facebook access token.
     *
     * @param string $accessToken
     * @return \App\User
     */
    protected function authWithFacebook($accessToken)
    {
        $account = Socialite::driver('facebook')->userFromToken($accessToken);
        $email = $account->email ?: $account->user['id'].'@spotfinder.mx';
        $user = User::updateOrCreate(
            ['fb_id' => $account->user['id']],
            ['email' => $email, 'name' => $account->name, 'registration_mode' => 'facebook',
             'password' => '']
        );

        return $user;
    }
}
