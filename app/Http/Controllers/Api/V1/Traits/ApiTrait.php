<?php

namespace App\Http\Controllers\Api\V1\Traits;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Model;

trait ApiTrait
{

    protected function checkUser($user)
    {
        if(!$user)
        {
            throw new AuthenticationException();
        }
    }

    protected function dispatchPagination($listName = 'data', $results)
    {

        $response = [
            'pagination' => [
                'total'        => $results->total(),
                'per_page'     => $results->perPage(),
                'current_page' => $results->currentPage(),
                'last_page'    => $results->lastPage()
            ],
            $listName => $results->items()
        ];

        return response()->json([
            'code' => 200,
            'data' => $response
        ]);
    }

    protected function dispatch($data)
    {
        return response()->json([
            'code' => 200,
            'data' => $data
        ]);
    }

    protected function dispatchSuccess()
    {
        return response()->json([
            'code' => 200
        ]);
    }

    protected function dispatchError($code = 500, $message = '', $userMessage = '')
    {
        return response()->json([
            'error' => [
                'debug' => $message,
                'user' => $userMessage ?: $message
            ]
        ], $code);
    }

}
