<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Api\V1\Traits\ApiTrait;

class ApiBaseController extends BaseController
{
    use ApiTrait;
}
