<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use \Validator;
use App\Models\User;
use App\Models\Device;
use App\Utilities\PushSender;
use App\Utilities\AppFiles;
use App\Http\Requests\Api\StoreUser;
use App\Http\Requests\Api\ProfileUpdate;
use App\Http\Requests\Api\UpdatePassword;
use App\Http\Requests\Api\UpdateFCMToken;
use App\Mail\RecoverUser;

use App\Repositories\DeviceRepository;
use App\Repositories\UserRepository;

use Mail;
use Auth;
use Hash;
use App;
use DB;

class AccountController extends ApiBaseController
{
    protected $device, $user;

    public function __construct(Request $request)
    {
        $this->middleware(
            'auth:api',
            [
                'except' => [
                    'store',
                    'recoverRequest',
                    'updatePassword',
                ],
            ]
        );

        $this->device = new DeviceRepository();
        $this->user = new UserRepository();
    }

    public function logout(Request $request)
    {
        $user = Auth::user();

        if ($request->has('device_id')) {
            $this->device->destroy($request->device_id);
        }

        $user->token()->revoke();

        return $this->dispatchSuccess();
    }

    public function profile(Request $request)
    {
        $user = Auth::user();

        if ($request->has('fcm_token') && $request->fcm_token != null) {
            $device = $this->device->create($request->all());
        }

        return $this->dispatch(
            [
                'user' => $user,
                'device_id' => isset($device) ? $device->id : null,
            ]
        );
    }

    public function store(StoreUser $request)
    {
        $user = $this->user->create($request->all());

        if ($request->get('image') != null) {
            $user->image = AppFiles::saveBase64Image($request->get('image'), 'users', $user->id.'-');
            $user->save();
        }

        return $this->dispatch(['user' => User::findOrFail($user->id)]);
    }

    public function updateFCMToken(UpdateFCMToken $request)
    {
        $device = Device::firstOrNew(
            ['id' => $request->device_id],
            [
                'user_id' => Auth::id(),
                'os' => strtolower($request->os),
            ]
        );

        $device->fcm_token = $request->token;

        $device->save();

        return $this->dispatch(
            [
                'device_id' => $device->id,
            ]
        );
    }

    public function profileUpdate(ProfileUpdate $request)
    {
        $user = Auth::user();

        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->has('password') && $request->has('last_password')) {
            if (!Hash::check($request->last_password, $user->password)) {
                return $this->dispatchError(
                    400,
                    'last_password does not match user password'
                    ,
                    'Your current password does not match the one you provided.'
                    ,
                    'Tu contraseña no coincide con la que ingresaste.'
                );
            }

            $user->password = $request->password;
        }

        if ($request->has('image')) {
            $user->image = AppFiles::saveBase64Image($request->get('image'), 'users', $user->id.'-');
        }

        $user->save();

        return $this->dispatch(
            [
                'user' => $user,
            ]
        );
    }

    public function recoverRequest(Request $request)
    {
        if (!$request->has('email')) {
            return $this->dispatchError(400);
        }


        if (!$user = User::getByEmail($request->get('email'))) {
            return $this->dispatchError(
                404,
                'There´s no user with that email.',
                'There´s no user with that email.',
                'No existe ningun usuario con ese correo electrónico.'
            );
        }


        if ($user->registration_mode != 'email') {
            return $this->dispatchError(
                404,
                'The user registration mode isn´t email',
                'You can´t recover the password because you didn´t register via email.',
                'No puedes recuperar tu contraseña por que no te registraste por medio de correo electrónico.'
            );
        }

        $user->recover_code = strtoupper(substr(uniqid(), 7, 4));
        $user->recover_before = DB::raw('DATE_ADD(NOW(), INTERVAL 30 MINUTE)');
        $user->save();

        // Send email
        Mail::to($user->email)->send(new RecoverUser($user));

        return $this->dispatch(
            [
                'message' => 'Se ha enviado un email de recuperación',
            ]
        );
    }

    public function updatePassword(UpdatePassword $request)
    {
        $newPassword = $request->get('new_password');

        if (!$user = User::getByRecoverCode($request->get('email'), strtoupper($request->get('recover_code')))) {
            return $this->dispatchError(
                404,
                'Invalid recover code.',
                'Invalid recover code.',
                'Código de recuperación invalido.'
            );
        }

        $user->recover_code = '';
        $user->save();
        $user->updatePassword($newPassword);

        return $this->dispatchSuccess();
    }
}
