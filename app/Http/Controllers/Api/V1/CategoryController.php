<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use \Validator;
use App\Models\Spot;
use App\Models\Category;
use Auth;

class CategoryController extends ApiBaseController
{

    public function index(Request $request)
    {
        return $this->dispatch([
            'categories' => Category::all()
        ]);
    }

    public function show(Category $category)
    {
        return $this->dispatch(['category' => $category]);
    }
}
