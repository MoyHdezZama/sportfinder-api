<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Models\Spot;
use App\Models\SpotImage;
use App\Models\Favorite;
use App\SpotFilter\SpotFilter;

use App\Http\Requests\Api\StoreSpot;
use App\Http\Requests\Api\AddComment;

use Auth;
use App;

class SpotController extends ApiBaseController
{

    public function __construct(Request $request)
    {
        $this->middleware(
            'auth:api',
            [
                'only' => [
                    'store',
                    'addComment',
                    'mySpots',
                    'userFavorites',
                    'addToFavorites',
                ],
            ]
        );
    }

    public function spotsFromDistance($request, $query = null)
    {
        if (!$query) {
            $query = (new Spot)->newQuery();
        }
        if ($request->has('lat') && $request->has('lng')) {
            $distance = 150;
            if ($request->has('max_distance')) {
                $distance = $request->max_distance;
            }
            $query = $query->geofencePaginated($request->lat, $request->lng, 0, $distance);
        }

        return $query;
    }

    public function spotsFromDistanceNoPagination($request, $query = null)
    {
        if (!$query) {
            $query = (new Spot)->newQuery();
        }
        if ($request->has('lat') && $request->has('lng')) {
            $distance = 150;
            if ($request->has('max_distance')) {
                $distance = $request->max_distance;
            }
            $query = $query->geofence($request->lat, $request->lng, 0, $distance);
        }

        return $query;
    }

    public function index(Request $request)
    {
        return $this->dispatchPagination('spots', $this->spotsFromDistance($request)->paginate(10));
    }

    public function mapSpots(Request $request)
    {
        $spots = $this->spotsFromDistanceNoPagination($request, SpotFilter::apply($request));

        if ($request->has('spots_ids')) {
            $ids = explode(',', $request->spots_ids);
            $spots->whereNotIn('id', $ids);
        }

        return $this->dispatch(['spots' => $spots->get()]);
    }

    public function show(Spot $spot)
    {
        return $this->dispatch(['spot' => $spot]);
    }

    public function filter(Request $request)
    {
        return $this->dispatchPagination(
            'spots',
            $this->spotsFromDistance($request, SpotFilter::apply($request))->paginate(10)
        );
    }

    public function store(StoreSpot $request)
    {
        $user = Auth::user();

        $spot = Spot::create($request->all() + ['user_id' => $user->id]);
        $spot->addImages($request->input('images'));
        $spot->save();

        return $this->dispatch(['spot' => $spot]);
    }

    public function mySpots(Request $request)
    {
        return $this->dispatchPagination(
            'spots',
            $this->spotsFromDistance($request, Auth::user()->spots())->paginate(10)
        );
    }

    public function userFavorites(Request $request)
    {
        return $this->dispatchPagination(
            'spots',
            $this->spotsFromDistance($request, Auth::user()->favorites())->paginate(10)
        );
    }

    public function addToFavorites(Spot $spot)
    {
        $user = Auth::user();

        $favorite = Favorite::where('user_id', '=', $user->id)
            ->where('spot_id', '=', $spot->id);

        if ($favorite->exists()) {
            $favorite->delete();
        } else {
            Favorite::create(
                [
                    'user_id' => $user->id,
                    'spot_id' => $spot->id,
                ]
            );
        }

        return $this->dispatch(['favorite' => $favorite->exists()]);

    }

    public function addComment(AddComment $request, Spot $spot)
    {
        $comment = $spot->addComment($request->get('comment'), Auth::id());

        return $this->dispatch(['comment' => $comment]);
    }

    public function getComments(Request $request, Spot $spot)
    {
        return $this->dispatchPagination('comments', $spot->comments()->paginate(10));
    }

    public function destroy(Spot $spot)
    {
        $user = Auth::user();

        if ($spot->user_id != $user->id) {
            return $this->dispatchError(400, 'The Spot does not belongs to the user', 'Spot not found');
        }
        //TODO: DELETE SPOT IMAGES
        $spot->images()->delete();
        $spot->delete();

        return $this->dispatchSuccess();
    }
}
