<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequest;

class ProfileUpdate extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes',
            'password' => 'sometimes|min:6|max:20s',
            'confirm_password' => 'same:password',
            'image' => 'sometimes',
        ];
    }
}
