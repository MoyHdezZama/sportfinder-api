<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequest;

class UpdatePassword extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|exists:users,email',
            'recover_code' => 'required',
            'new_password' => 'required|min:6|max:20'
        ];
    }
}
