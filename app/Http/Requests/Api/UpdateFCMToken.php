<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequest;

class UpdateFCMToken extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'device_id' => 'nullable|sometimes',
            'os' => 'required',
        ];
    }
}
