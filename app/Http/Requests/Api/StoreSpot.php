<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequest;

class StoreSpot extends ApiRequest
{

   
 /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    { 
       return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'min' => 'The title must to be at least 20 characters long.',
            'required' => 'All fields are required.',
            'exists' => 'Category not found.',
            'regex' => 'The latitude format is invalid.'
        ];
    }
}
