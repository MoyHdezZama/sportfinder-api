<?php

namespace App\Observers;

use App\Models\Device;

class DeviceObserver
{

    public function saving(Device $device)
    {
        $device->os = strtolower($device->os);
    }
}