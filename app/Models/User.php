<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Hash;
use App\Utilities\AppFiles;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /*
    Password grant client created successfully.
    Client ID: 1
    Client Secret: zlyg479hBtXjYrEicwHJ3jnRKcM2ZG5QjVfHnIcq
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'fb_id',
        'image',
        'registration_mode',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at',
    ];

    public static function getByEmail($email)
    {
        return static::where("email", $email)->first();
    }

    public static function getByRecoverCode($email, $recoverCode)
    {
        return static::where('email', $email)->where('recover_code', $recoverCode)
            ->where('recover_before', '>', DB::raw('NOW()'))->first();
    }

    ///******  Attributes* ******
    public function getImageAttribute($value)
    {
        return AppFiles::getImageUrl($value, "");
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    ///******* Functions ******

    public function updatePassword($newPassword)
    {
        $this->password = $newPassword;
        $this->save();
    }

    //**** Relationships *********

    public function spots()
    {
        return $this->hasMany(Spot::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Spot::class, 'favorites', 'user_id', 'spot_id');
    }
}
