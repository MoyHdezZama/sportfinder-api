<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utilities\AppFiles;

class Category extends Model
{

    protected $hidden = ['created_at', 'updated_at'];

    public function getImageAttribute($value)
    {
        return AppFiles::getImageUrl($value);
    }

    // **** Relationships *****
    public function spots() {
          return $this->hasMany(Spot::class);
    }
}
