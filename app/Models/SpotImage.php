<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utilities\AppFiles;

class SpotImage extends Model
{
      protected $fillable = ['spot_id','image'];

      ///******  Attributes* ******
      public function getImageAttribute($value)
      {
          return AppFiles::getImageUrl($value);
      }
}
