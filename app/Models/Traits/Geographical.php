<?php
namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Spot;
use DB;

trait Geographical
{
    /**
     * @param Builder $query
     * @param float $latitude Latitude
     * @param float $longitude Longitude
     * @return Builder
     */
    public function scopeDistance($query, $latitude, $longitude)
    {
        $latName = $this->getQualifiedLatitudeColumn();
        $lonName = $this->getQualifiedLongitudeColumn();
        $query->select($this->getTable() . '.*');
        $sql = "((ACOS(SIN(? * PI() / 180) * SIN(" . $latName . " * PI() / 180) + COS(? * PI() / 180) * COS(" .
            $latName . " * PI() / 180) * COS((? - " . $lonName . ") * PI() / 180)) * 180 / PI()) * 60 * ?) as distance";
        $kilometers = false;
        if (property_exists(static::class, 'kilometers')) {
            $kilometers = static::$kilometers;
        }
        if ($kilometers) {
            $query->selectRaw($sql, [$latitude, $latitude, $longitude, 1.1515 * 1.609344]);
        } else {
            // miles
            $query->selectRaw($sql, [$latitude, $latitude, $longitude, 1.1515]);
        }
        //echo $query->toSql();
        //var_export($query->getBindings());
        return $query->orderBy('distance', 'asc');
    }

    public function scopeGeofence($query, $latitude, $longitude, $inner_radius, $outer_radius)
    {
        $query = $this->scopeDistance($query, $latitude, $longitude);
        //TODO: DO THIS PROPERLY, LARAVEL PAGINATION AND HAVING CANT GET TOGETHER
        $ids = $query->havingRaw('distance BETWEEN ? AND ?', [$inner_radius, $outer_radius])->pluck('id');

        //TODO: WHERE IN AND EMPTY ARRAY
        if ($ids->count() > 0)
        {
            $ids_ordered = implode(',', $ids->toArray());

            return Spot::whereIn('id', $ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"));
        }

        return $query;
    }

    public function scopeGeofencePaginated($query, $latitude, $longitude, $inner_radius, $outer_radius)
    {
        $query = $this->scopeDistance($query, $latitude, $longitude);

        $ids = $query->havingRaw('distance BETWEEN ? AND ?', [$inner_radius, $outer_radius])->pluck('id');

        $ids_ordered = implode(',', $ids->toArray());

        return Spot::whereIn('id', $ids)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"));

        return $query;
    }

    protected function getQualifiedLatitudeColumn()
    {
        return $this->getTable() . '.' . $this->getLatitudeColumn();
    }
    protected function getQualifiedLongitudeColumn()
    {
        return $this->getTable() . '.' . $this->getLongitudeColumn();
    }
    public function getLatitudeColumn()
    {
        return defined('static::LATITUDE') ? static::LATITUDE : 'latitude';
    }
    public function getLongitudeColumn()
    {
        return defined('static::LONGITUDE') ? static::LONGITUDE : 'longitude';
    }
}
?>
