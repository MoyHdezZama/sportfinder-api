<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'spot_id', 'comment'];

    protected $hidden = ['updated_at', 'user_id', 'spot_id'];

    protected $appends = ['user'];

    ////  ******Attributes ********
    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    public function spot()
    {
        return $this->belongsTo(Spot::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }

    //***** Relationships ******

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
