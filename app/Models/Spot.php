<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Geographical;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use DB;
use App\Utilities\AppFiles;

class Spot extends Model
{

    use Geographical;

    protected $fillable = ['title','summary','lat','lng', 'user_id', 'category_id', 'difficulty'];

    protected $hidden = ['pivot', 'user_id', 'category_id', 'created_at', 'updated_at'];

    protected $appends = ['user', 'favorite', 'images', 'comments', 'category', 'favorite_count', 'comments_count'];
    const LATITUDE  = 'lat';
    const LONGITUDE = 'lng';
    protected static $kilometers = true;

    //*****ATTRIBUTES******


    public function getFavoriteAttribute()
    {
        $user = Auth::user();
        if (!$user)
        {
            return false;
        }

        return Favorite::where('user_id', '=', $user->id)
        ->where('spot_id', '=', $this->id)->exists();
    }

    public function getFavoriteCountAttribute()
    {
        return Favorite::where('spot_id', $this->id)->count();
    }

    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }

    public function getCommentsAttribute()
    {
        return array();
    }

    public function getImagesAttribute()
    {
        return $this->images()->pluck('image');
    }

    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    public function getCategoryAttribute()
    {
        return $this->category()->first();
    }

    public function getLatAttribute($value)
    {
        return (double)$value;
    }

    public function getLngAttribute($value)
    {
        return (double)$value;
    }

    protected function getQualifiedLatitudeColumn()
    {
        return $this->getTable() . '.' . $this->getLatitudeColumn();
    }
    protected function getQualifiedLongitudeColumn()
    {
        return $this->getTable() . '.' . $this->getLongitudeColumn();
    }
    public function getLatitudeColumn()
    {
        return defined('static::LATITUDE') ? static::LATITUDE : 'lat';
    }
    public function getLongitudeColumn()
    {
        return defined('static::LONGITUDE') ? static::LONGITUDE : 'lng';
    }

    /**
     * @param Builder $builder
     * @return string
     */
    private static function getSql($builder)
    {
        $sql = $builder->toSql();
        foreach($builder->getBindings() as $binding)
        {
            $value = is_numeric($binding) ? $binding : "'".$binding."'";
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }
        return $sql;
    }

    public function addImages($images)
    {
        $imagesArray = explode(' ', $images);
        foreach ($imagesArray as $image) {
            $path = "images/spots/spot_" . $this->id . "_" . uniqid() . ".jpeg";

            $img = Image::make($image);
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $img->save($path);


            SpotImage::create([
                'spot_id' => $this->id,
                'image' => $path
            ]);
        }
    }

    public function addComment($comment, $userId)
    {
        $comment = Comment::create([
            'spot_id' => $this->id,
            'comment' => $comment,
            'user_id' => $userId
        ]);

        return $comment;
    }

    // ******* Relationships ********

    public function images()
    {
        return $this->hasMany(SpotImage::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function favorite()
    {
        return $this->hasMany(Favorite::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
