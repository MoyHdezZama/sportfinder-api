<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
      protected $fillable = ['user_id','spot_id'];

      /**
      * The attributes that should be hidden for arrays.
      *
      * @var array
      */
      protected $hidden = [
            'created_at', 'updated_at',
      ];
}
