<?php

namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Device;

use Auth;

class DeviceRepository extends Repository
{
    public function __construct()
    {
        $this->model = new Device();
    }

    public function create(array $data)
    {
        $data["user_id"] = isset($data["user_id"]) ? $data["user_id"] : Auth::user()->id;

        return parent::create($data);
    }
}