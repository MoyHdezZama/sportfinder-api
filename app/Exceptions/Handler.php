<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \League\OAuth2\Server\Exception\OAuthServerException::class,
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
         if ($request->expectsJson()) {
             return response()->json([
                 'error' => [
                     'debug' => 'Unauthenticated. Token or refresh token is invalid.',
                     'user' => App::getLocale() == 'es' ? 'Por favor inicie sesión' : 'Please Log in'
                 ]
             ], 401);
         }

         return redirect()->guest(route('login'));
    }

     /**
      * Convert an validation exception into an error response.
      *
      * @param  \Illuminate\Validation\ValidationException  $e
      * @return \Illuminate\Http\Request $request
      */
     public function convertValidationExceptionToResponse(ValidationException $e, $request)
     {
         if($e instanceof ValidationException && $request->expectsJson())
         {
             // Get first error
             $error = array_values($e->errors())[0][0];

             // Response json
             return response()->json([
                 'error' => [
                     'debug' => $error,
                     'user' => $error
                 ]
             ], 400);
         }
         else
         {
             parent::convertValidationExceptionToResponse($e, $request);
         }
     }

     public function render($request, Exception $e)
     {
         if ($e instanceof ModelNotFoundException)
         {
             if($request->is('api/spots/*'))
             {
                 return $this->dispatchError(404, App::getLocale() == 'es' ? 'Spot no encontrado' : 'Spot not found');
             }
             else if($request->is('api/v1/categories/*'))
             {
                 return $this->dispatchError(404, App::getLocale() == 'es' ? 'Categoria no encontrada' : 'Category not found');
             }

             return $this->dispatchError(404, App::getLocale() == 'es' ? 'Recurso no encontrado' : 'Resource not found');
         }

         return parent::render($request, $e);
     }

     public function dispatchError($code = 500, $message = '', $userMessage = '')
     {
         return response()->json([
             'error' => [
                 'debug' => $message,
                 'user' => $userMessage ?: $message
             ]
         ], $code);
     }
}
