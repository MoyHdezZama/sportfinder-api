<?php

namespace App\Utilities;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM as FCM;
use App\Models\Device;

class PushSender
{

    static public function sendToUser(\App\Models\User $user, $title, $content, $data = null)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setPriority('high');
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($content)
        ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $tokensIOS = $user->devices->where('user_id',$user->id)->where('os','ios')->pluck('fcm_token')->toArray();
        $tokensAndroid = $user->devices->where('user_id',$user->id)->where('os','android')->pluck('fcm_token')->toArray();

        if(count($tokensIOS) > 0)
        {
            $downstreamResponseIOS = FCM::sendTo($tokensIOS, $option, $notification, $data);
            static::handle($downstreamResponseIOS);
        }

        if(count($tokensAndroid) > 0)
        {
            $downstreamResponseAndroid = FCM::sendTo($tokensAndroid, $option, null, $data);
            static::handle($downstreamResponseAndroid);
        }
    }

    static public function sendToDevices($devices, $title, $content, $data = null)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setPriority('high');
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($content)
        ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $tokensIOS = $devices->where('os','ios')->pluck('fcm_token')->toArray();
        $tokensAndroid = $devices->where('os','android')->pluck('fcm_token')->toArray();

        if(count($tokensIOS) > 0)
        {
            $downstreamResponseIOS = FCM::sendTo($tokensIOS, $option, $notification, $data);
            static::handle($downstreamResponseIOS);
        }

        if(count($tokensAndroid) > 0)
        {
            $downstreamResponseAndroid = FCM::sendTo($tokensAndroid, $option, null, $data);
            static::handle($downstreamResponseAndroid);
        }
    }



    static public function handle($downstreamResponse)
    {
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        foreach ($downstreamResponse->tokensToDelete() as $token) {
            Device::where('fcm_token',$token)->delete();
        }

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        foreach ($downstreamResponse->tokensToModify() as $token) {
            Device::where('fcm_token',$token['oldToken'])->update('fcm_token',$token['newToken']);
        }

        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();

        dd($downstreamResponse);
    }

}
