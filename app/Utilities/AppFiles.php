<?php

namespace App\Utilities;

class AppFiles
{

    public static function saveFile($filePath, $extension, $folder = "", $prefix = "")
    {
        $data = file_get_contents($filePath);
        $filename = "";
        $newFilePath = "";
        $exists = true;

        while ($exists)
        {
            $filename = "files/" . (($folder) ? ($folder . "/") : "" ) . (($prefix) ? ($prefix . "-") : "" ) .
                number_format(microtime(true) * 10000, 0, '.', '') . "." . $extension;
            $newFilePath = public_path() . "/" . $filename;
            $exists = file_exists($newFilePath);
        }

        file_put_contents($newFilePath, $data);
        return $filename;
    }

    public static function saveBase64Image($base64_string, $folder = "", $prefix = "")
    {
        $random = uniqid($prefix);
        $data = explode(',', $base64_string);
        $ext = "";
        if ($data[0] == "data:image/png;base64")
        {
            $ext = "png";
        }
        else if ($data[0] == "data:image/jpg;base64" || $data[0] == "data:image/jpeg;base64")
        {
            $ext = "jpg";
        }
        if (!$ext)
        {
            return false;
        }

        $newFilename =  "images/" . (($folder) ? ($folder . "/") : "" ) . $random . "." . $ext;
        $newFilePath = public_path() . "/" . $newFilename;

        file_put_contents($newFilePath, base64_decode($data[1]));
        return $newFilename;
    }

    public static function getImageUrl($image, $path = "")
    {
        return config("app.url") . $path . $image;
    }

    public static function removeFile($filename)
    {
        unlink(public_path() . "/files/" . $filename);
    }

}
