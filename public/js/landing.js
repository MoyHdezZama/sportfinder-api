$(document).ready(function () {
    $("#nav-characteristics").on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".introduction1").offset().top
        }, 300);
    });

    $("#nav-how-works").on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".section2").offset().top
        }, 300);
    });

    $("#nav-contact").on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $(".contact").offset().top
        }, 300);
    });
});