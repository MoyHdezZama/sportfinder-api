<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotImagesTable extends Migration
{
      /**
      * Run the migrations.
      *
      * @return void
      */
      public function up()
      {
            Schema::create('spot_images', function (Blueprint $table) {
                  $table->increments('id');
                  $table->integer('spot_id')->unsigned();
                  $table->longText('image');
                  $table->timestamps();


                  $table->foreign('spot_id')->references('id')->on('spots');
            });
      }

      /**
      * Reverse the migrations.
      *
      * @return void
      */
      public function down()
      {
            Schema::dropIfExists('spot_images');
      }
}
