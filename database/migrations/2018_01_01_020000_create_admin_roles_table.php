<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;
use App\Models\Role;

class CreateAdminRolesTable extends Migration
{

    public function up()
    {
        Schema::dropIfExists('roles');
        Schema::create('roles', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title', 50);
            $table->string('status', 30)->default('active');

            $table->timestamps();
            $table->softDeletes();
        });

        // Seed table

        $roles = [
            'User',
            'Guest',
        ];

        DB::table('roles')->delete();
        foreach ($roles as $role)
        {
            Role::create([
                'title' => $role
            ]);
        }

        // Update user table

        Schema::table('users', function(Blueprint $table)
        {
            $table->integer('role_id')->unsigned()->default(1)->after('id');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropForeign('users_role_id_foreign');
            $table->dropColumn(['role_id']);
        });

        Schema::dropIfExists('roles');
    }

}
